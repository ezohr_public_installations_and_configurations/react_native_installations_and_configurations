# React Native Installations And Configurations

## Prerequisites

> Installed: Node.js ([Downloads](https://nodejs.org/en/download/current/))
>
> Installed: Visual Studio Code ([Download Visual Studio Code](https://code.visualstudio.com/Download))

## Expo Command Line Interface ([Installation](https://reactnative.dev/docs/environment-setup))

### macOS Terminal Application (Expo Command Line Interface)

`sudo npm i -g expo-cli`

`expo init [Project]`

> Select: blank (Typescript)

### Visual Studio Code Application (Expo Command Line Interface Part 1)

> Edit: .gitignore
>
> Enter: ([.gitignore](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/ReactNativeInstallationsAndConfigurations/-/blob/master/.gitignore))

```sh
# Excluding JavaScript And Map Files (Using TypeScript)
*.js
*.map
```

### Visual Studio Code Application (Expo Command Line Interface Part 2)

> Select: [Extensions]
>
> Search: ES7 React/Redux/GraphQL/React-Native Snippets (dsznajder)
>
> Select: Install
